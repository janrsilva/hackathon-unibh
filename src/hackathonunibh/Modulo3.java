/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackathonunibh;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jande
 */
public class Modulo3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int entrada = 4;
        String texto = "Texto #3";
        List<String> textoList = new ArrayList<>(Arrays.asList(texto.split("")));
        textoList = passo1(textoList);
        
    }

    private static List<String> passo1(List<String> texto){
        for(int i = 0; i < texto.size(); i++){
            byte[] bytes = texto.get(i).getBytes(StandardCharsets.US_ASCII);
            int cp = String.valueOf(texto.get(i)).codePointAt(0);
            texto.set(i, String.valueOf((char) (cp + 3)));
            System.out.print(String.valueOf((char) (cp + 3)));
        }
        return texto;
    }
}
