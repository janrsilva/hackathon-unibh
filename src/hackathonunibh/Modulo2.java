/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackathonunibh;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jande
 */
public class Modulo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Informe a entrada:");
        Scanner entrada = new Scanner(System.in);
        String senha = entrada.next();
        verificaSenha(senha);
    }

    private static void verificaSenha(String senha) {
        Pattern letrasMinusculas = Pattern.compile("([a-z]+)");
        Pattern letrasMaisculas = Pattern.compile("([A-Z]+)");
        Pattern numeros = Pattern.compile("([0-9]+)");
        Pattern restricoes = Pattern.compile("^([a-z]+|[A-Z]+|[0-9]+)+$");

        if(senha.length() >= 6 
                &&  senha.length() <= 32 
                && letrasMinusculas.matcher(senha).find()
                && letrasMaisculas.matcher(senha).find()
                && numeros.matcher(senha).find()
                && restricoes.matcher(senha).find()
                ){
            System.out.println("Senha valida");
        } else {
            System.out.println("Senha invalida");
        }
    }

}
