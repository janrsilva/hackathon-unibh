/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackathonunibh;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author jande
 */
public class Modulo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Informe a entrada:");
        Scanner entrada = new Scanner(System.in);
        String desafio = entrada.next();
        encontraRepeticao(desafio.split(""));
    }

    private static List<String> encontraRepeticao(String[] desafio) {
        List<String> repeticao = new ArrayList<>();
        String bufferRepeticao = "";
        String bufferDiferenca = "";
        for(int i = 0; i < desafio.length; i++){
            try{
                if(desafio[i].equals("1")){
                    repeticao.add("11");
                } else if(desafio[i].equals(desafio[i + 1]) && bufferDiferenca.isEmpty()){
                    bufferRepeticao += desafio[i];
                } else if(!bufferRepeticao.isEmpty()){
                    repeticao.add(bufferRepeticao + desafio[i]);
                    bufferRepeticao = "";
                } else if(!desafio[i].equals(desafio[i+ 1])){
                    bufferDiferenca += desafio[i];
                } else if(!bufferDiferenca.isEmpty()){
                    repeticao.add("1" + bufferDiferenca + "0");
                    bufferDiferenca = "";
                    i--;
                }
            } catch (ArrayIndexOutOfBoundsException e ){
                if(!bufferRepeticao.isEmpty()){
                    repeticao.add(bufferRepeticao + desafio[i]);
                }
                if(!bufferDiferenca.isEmpty()){
                    repeticao.add("1" + bufferDiferenca + desafio[i] + "0");
                }
            }
        }
        String reposta  = "";
        for(String repete: repeticao){
            if(repete.contains("1")){
                reposta += repete;
            } else {
                reposta += repete.length()+repete.split("")[0];
            }
        }
        System.out.println(reposta);
        return repeticao;
    }
    
}
